# Lean Delen

I am flutter developer trainee based in the Philippines 

I am a backend developer using PHP. I become excited in learning new things that the internet could offer and learning new things from people who have a lot of experienced in the IT industry.

# Contact Information

LinkedIn: [https://www.linkedin.com/in/lean-delen-005921210/](https://www.linkedin.com/in/lean-delen-005921210/)

# Work Experience

## Video Editor

Destiny Church Manila, March 2020 to Present

- Currently doing some video editing and training other video editor.

## Programmer Trainee

FFUF, July 21 to August 31, 2021

- I was trained on how mobile application development works
- I was worked with a pet project that involved converting a given design sample into actual UI
- I was trained the basic Java Script OOP
- I was also trained in testing UI.

# Skills

## Technical Skills

- Programming
- Video Editing
- Software Engineering
- Database Designing

## Soft Skills

- Open-mindedness
- Team Player
- Communication
- Creative

# Education

STI College Cubao 2016-2021